"use strict";
Object.prototype.log = function () {
    console.log(this.toString());
};
const x = 2;
const y = 3;
const z = 4;
console.log(x); // 2
console.log(y); // 3
console.log(z); // 4
x.log(); // 2
y.log(); // 3
z.log(); // 4
const cli = {
    nome: 'Pedro',
    toString() { return this.nome; }
};
cli.log(); // Pedro
