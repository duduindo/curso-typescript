

function echo(objeto: any) {
  return objeto;
}

console.log(echo('João').length);
console.log(echo(27).length);
console.log(echo({ nome: 'João', idade: 27 }));



function echoMelhorado<T>(objeto: T): T {
  return objeto;
}

console.log(echoMelhorado('João').length);
//console.log(echoMelhorado(27).length); // Erro: error TS2339: Property 'length' does not exist on type '27'.
console.log(echoMelhorado<number>(27));
console.log(echoMelhorado({ nome: 'João', idade: 27 }));



// Generics disponíveis na API
let avaliacoes: Array<number> = [10, 9.3, 7.7];

avaliacoes.push(6.4);
// avaliacoes.push('5.5');
console.log(avaliacoes);


function imprimir<T>(args: T[]): void {
  args.forEach(elemento => console.log(elemento));
}


imprimir([1, 2, 3, 4]);
imprimir<number>([1, 2, 3, 4]);
imprimir<string>(['Fulano', 'Cicrano', 'Beltrano']);
imprimir<{ nome: string, idade: number }>([
  { nome: 'Fulano', idade: 22 },
  { nome: 'Cicrano', idade: 23 },
  { nome: 'Beltrano', idade: 24 }
])

type Aluno = { nome: string, idade: number };

imprimir<Aluno>([
  { nome: 'Fulano', idade: 22 },
  { nome: 'Cicrano', idade: 23 },
  { nome: 'Beltrano', idade: 24 }
])


// Tipo Genérico
type Echo = <T>(data: T) => T;
const chamarEcho: Echo = echoMelhorado;
console.log( chamarEcho<string>('Alguma coisa') );




// Class com Generics

abstract class OperacaoBinaria<TIPO, RETORNO> {
  constructor(public operando1: TIPO, public operando2: TIPO) {}

  abstract executar(): RETORNO
}

class SomaBinaria extends OperacaoBinaria<number, number> {
  executar(): number {
    return this.operando1 + this.operando2;
  }
}
console.log(new SomaBinaria(3, 4).executar());           // 7






class Data {
  constructor(public dia: number = 1, public mes: number = 1, public ano: number = 1970) {}
}


class DiferencaEntreDatas extends OperacaoBinaria<Data, string> {
  getTime(data: Data): number {
    let { dia, mes, ano } = data;

    return new Date(`${mes}/${dia}/${ano}`).getTime();
  }

  executar(): string {
    const t1 = this.getTime(this.operando1);
    const t2 = this.getTime(this.operando2);
    const diferenca = Math.abs(t1 - t2);
    const dia = 1000 * 60 * 60 * 24; // milissegundos em um dia

    return `${Math.ceil(diferenca / dia)} dia(s)`;
  }
}


const d1 = new Data(1, 2, 2020);
const d2 = new Data(5, 2, 2020);

console.log(new DiferencaEntreDatas(d1, d2).executar())



// Exemplo Restrição a string
class Fila<T extends string> {
  constructor(public a: T) {}

  getContent(): T {
    return this.a;
  }
}


const fila = new Fila<string>('Fila ABC');

console.log( fila.getContent() )
