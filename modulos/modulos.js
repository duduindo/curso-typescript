"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const retangulo_1 = __importDefault(require("./retangulo"));
const circunferencia_1 = __importDefault(require("./circunferencia"));
console.log('Módulo carregado...');
console.log(retangulo_1.default(10, 20)); // 200
console.log(circunferencia_1.default(10)); // 314
const { digaOi } = require('./novo');
console.log(digaOi('Ana'));
