import retangulo from './retangulo';
import circunferencia from './circunferencia';

console.log('Módulo carregado...')
console.log( retangulo(10, 20) ) // 200
console.log( circunferencia(10) ) // 314

const { digaOi } = require('./novo');

console.log(digaOi('Ana'))
