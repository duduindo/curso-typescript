# Curso TypeScript

**Udemy** https://www.udemy.com/typescript-pt/


### Básicos de Tipos
Aula: https://www.udemy.com/typescript-pt/learn/v4/t/lecture/14049815

```ts
// string
let name = 'João';

name = 28; // Causará um erro de tipo no TypeScript

name = 'Fulano'; // O certo seria este
```


### Tipos numéricos e Booleano

```ts
// numbers
let idade = 27

idade = 27.5 // É válido


// Boolean
let possuiHobbies = false

possuiHobbies = true

```

### Tipos explícitos

`any` não é muito recomendado, porque fica igual o JavaScript

```ts
/**
  É a msm coisa:

  let minhaIdade: any
*/
let minhaIdade; // Virou tipo "Any" automaticamente

minhaIdade = 27; // É  válido
minhaIdade = 'Vinte e sete'; // É  válido
```

```ts
let minhaIdade: any;

minhaIdade = 27;
minhaIdade = 'Vinte e sete'; // É  válido
```


### Arrays e tipos

É inválido, porque virou um array com numbers e não de strings como declarado acima

```ts
let hobbies = ['Cozinhar', 'Praticar Esportes'];

hobbies = [100]; // É inválido, porque virou um array de com numbers e não de strings 
                 // como declarado acima
```

Para solucionar e deixar o array flexível:

```ts
let hobbies: any[] = ['Cozinhar', 'Praticar Esportes'];

hobbies = [100]; // Validou

hobbies = 100; // Erro aqui
```


### Tuplas

Tuplas é um array de quantidade pré-definidas de tipos

Inválido, porque a ordem dos tipos array está incorreta:

```ts
let endereco: [string, number] = ['Av Prinicipal', 99, 123]; // Inválido: Faltou um

let endereco: [number, string] = ['Av Prinicipal', 99]; // Inválido: Está fora de ordem

endereco = ['Bloco A', 1260]; // Inválido
```

Correto:

```ts
let endereco: [string, number, string] = ['Av Prinicipal', 99, ''];

endereco = ['Rua importante', 1260, 'Bloco A'];
```


### Enums

Como não há valor definido, o `Cinza` representará `0`, `Verde` `1`, etc...

```ts
enum Cor {
  Cinza, // 0
  Verde, // 1 
  Azul   // 2
};


let minhaCor: Cor = Cor.Verde;

console.log(minhaCor);  // 1
```

Com apenas um valor definido:
*O Azul virou 101 e Vermelho 102*

```ts
enum Cor {
  Cinza,        // 0
  Verde = 100,  // 100
  Azul,         // 101
  Vermelho,     // 102
  Laranja = 100 // 100  // É válido repitir valores
};


let minhaCor: Cor = Cor.Verde;

console.log(minhaCor);  // 100
```

### Any

O `Any` é essencial para quem está emigrando para TypeScript

```ts
let carro: any = 'BMW'

carro = { marca: 'BMW', ano: 2019 }
```


### Usando Tipos em Funções (Parâmetros e Retorno)

```ts
const nome = 'Eduardo';

function retornaMeuNome(): string {
  return nome;
}

console.log( retornaMeuNome() ); // Eduardo
```

Erro! Retornou number

```ts
const nome = 'Eduardo';
const minhaIdade = 27;

function retornaMeuNome(): string {
  return minhaIdade; // Erro! Retornou number
}

console.log( retornaMeuNome() );  
```

#### Void - *Vazio*
Função que não retorna nada

```ts
function digaOi(): void {
  console.log('Oi');
}
```

Retornar algo com `:void`

```ts
function digaOi(): void {
  console.log('Oi');

  return minhaIdade; // Erro!
}
```

#### Com parâmetros

Inválido, porque os parâmetros estão definidos como `any`: <br>

```bash
Parameter 'numA' implicitly has an 'any' type.
Parameter 'numB' implicitly has an 'any' type.
```

```ts
function multiplicar(numA, numB): number {
  return numA * numB;
}

multiplicar(1, 2);
```

Solucionando:

```ts
function multiplicar(numA: number, numB: number): number {
  return numA * numB;
}

multiplicar(1, 2);   // Válido
multiplicar(4.5, 2); // Válido
multiplicar(4, '2'); // Inválido
```


### Funções como tipos

```ts
const teste = function(a: number, b: number): boolean {
  return false;
}

teste(1, 1); // Válido
```

#### Tipo função

O que importa é o tipo dos parâmetros e não o nome dos parâmetros

```ts
function multiplicar(numA: number, numB: number): number {
  return numA + numB;
}

let calculo: (numeroA: number, numeroB: number) => number

calculo = multiplicar;

console.log( calculo(5, 6) ) // 11
```


### Objetos e Tipos

Inválido, porque está faltando a propriedade `idade` no objeto

```ts
let usuario = {
  nome: 'João',
  idade: 27
};

usuario = {
  nome: 'opa', 
};  // Error
```

#### Explicitando as propriedades e tipos:

```ts
let usuario: { nome: string, idade: number } = {
  nome: 'João',
  idade: 27
};
```


### Definindo Tipos Personalizados (Alias)


#### O problema - a duplicação

Duplicação de tipos:

```ts

let funcionario: {  
  supervisores: string[],
  baterPonto: (horas: number) => string
} = {
  supervisores: ['Ana', 'Fernando'],
  baterPonto(horario: number): string {
    if (horario <= 8) {
      return 'Ponto normal'
    } else {
      return 'Fora do horário!'
    }
  }
};

// Duplicação de tipos: supervisores e baterPonto
let funcionario2: {  
  supervisores: string[],
  baterPonto: (horas: number) => string
} = {
  supervisores: ['Fulana', 'Sicrano'],
  baterPonto(horario: number): string {
    if (horario <= 8) {
      return 'Ponto normal'
    } else {
      return 'Fora do horário!'
    }
  }
};
```

#### A solução

```ts
// Alias
type Funcionario = {
  supervisores: string[],
  baterPonto: (horas: number) => string
};


let funcionario: Funcionario = {
  supervisores: ['Ana', 'Fernando'],
  baterPonto(horario: number): string {
    if (horario <= 8) {
      return 'Ponto normal'
    } else {
      return 'Fora do horário!'
    }
  }
};

let funcionario2: Funcionario = {
  supervisores: ['Fulana', 'Sicrano'],
  baterPonto(horario: number): string {
    if (horario <= 8) {
      return 'Ponto normal'
    } else {
      return 'Fora do horário!'
    }
  }
}

```

### Múltiplos Tipos com Union Types

```ts
let nota: number | string = 10;

nota = 'Dez'; // Válido
nota = 10;    // Válido
```


### O Tipo "Never"

`never` é um tipo que indica que a função nunca irá retornar nada, tipo um loop infinito:

```ts
function falha(msg: string): never {
  while(true) {} // loop infinito
}

// ou
function falha(msg: string): never {
  throw new Error(msg);
}

```

### Valores Opcionais com Tipo Null

(Não entendi nada sobre o tema)

```ts
let altura = 12;
// altura = null; // Error

let alturaOpcional: null | number = 12;
alturaOpcional = null;


type Contato = {
  nome: string,
  tel1: string,
  tel2: string | null
};

const contato1: Contato = {
  nome: 'Fulano',
  tel1: '9999-8888',
  tel2: null
};


let podeSerNulo = null; // Aqui é um tipo Any
let podeSerNulo2: null = null; // Aqui é um tipo Null


let a = null;

a = 10;
a = '10';
```

### Evitando "Any" implícito - Flag ou configuração - noImplicitAny

Com `false`, o compilador vai ignorar os "any". 

**Recomendado** na transição do JS para TS

Arquivo: **tsconfig.json**

```json
{
  "compilerOptions": {  
    "noImplicitAny": false
  }
}
```

Sem erro nos parâmetros:

```ts
function teste(a, b) {   // O compilador não vai soltar um alerta
}
```


### "strictNullChecks", "noUnusedParameters" e "noUnusedLocals"

#### strictNullChecks

Com `false`, o compilador não irá emitir o erro abaixo

```json
{
  "compilerOptions": {  
    "strictNullChecks": true 
  }
}
```

```ts
function saudar(isManha: boolean) {
  let saudacao: string

  if (isManha) {
    saudacao = 'Bom dia!'
  }

  return saudacao; // index.ts:13:10 - error TS2454: Variable 'saudacao' is used before being assigned.
}
```

#### noUnusedParameters

Com `false`, o compilador não irá emitir o erro abaixo

```json
{
  "compilerOptions": {  
    "noUnusedParameters": true
  }
}
```

```ts
function teste(numero: number) { // Erro: 'numero' nunca foi usado
}
```


#### noUnusedLocals

Com `false`, o compilador não irá emitir o erro abaixo

```json
{
  "compilerOptions": {  
    "noUnusedLocals": true
  }
}
```

```ts
function testeLocal() { // Erro: 'a' nunca foi usado
  let a = 1;  
} // index.ts:17:16 - error TS6133: 'numero' is declared but its value is never read.
```

### Parâmetro Padrão

Simples:

```ts
function contagemRegressiva(inicio: number = 10): void {
  console.log(inicio)
}

contagemRegressiva() // Log: 10
```

Com subtração no parâmetro padrão. **OBS:** Isto é nativo do JS

```ts
function contagemRegressiva(
  inicio: number = 10,
  fim: number = inicio - 3
): void {
  console.log(inicio, fim)
}


contagemRegressiva() // 10, 3
```

### Classes & Atributos #01

```ts
class Data {
  // Público por padrão
  dia: number
  public mes: number // Apenas exemplo
  ano: number

  constructor(dia: number = 1, mes: number = 1, ano: number = 1970) {
    this.dia = dia;
    this.mes = mes;
    this.ano = ano;
  }
}


const aniversario = new Data(3, 11, 1991);
aniversario.dia = 4;

console.log(aniversario.dia) // 4
```

### Classes & Atributos #02

A forma mais enxuta de declarar propriedades no `this`

```ts
class Data {
  constructor(public dia: number = 1, public mes: number = 1, public ano: number = 1970) {

  }
}

const aniversario = new Data(3, 11, 1991);
aniversario.dia = 4;

console.log(aniversario.dia) // 4
```

### Classes & Métodos

```ts
class Produtos {
  constructor(public nome: string, public preco: number, public desconto: number = 0) {

  }

  public resumo(): string {
    return `${this.nome} custa R$${this.preco} (${this.desconto * 100}% off)`;
  }
}


const prod1 = new Produtos('Caneta Bic Preta', 4.20);
prod1.desconto = 0.06;
console.log(prod1.resumo()); // Caneta Bic Preta custa R$4.2 (6% off)

const prod2 = new Produtos('Caderno escolar', 18.80, 0.15);
console.log(prod2.resumo()); // Caderno escolar custa R$18.8 (15% off)
```

### Modificadores de acesso

```ts

class Carro {
  private velocidadeAtual: number = 0;

  constructor(public marca: string, public modelo: string, private velocidadeMaxima: number = 200) {}

  private alterarVelocidade(delta: number): number {
    const novaVelocidade = this.velocidadeAtual + delta;
    const velocidadeValida = novaVelocidade >= 0 && novaVelocidade <= this.velocidadeMaxima;

    if (velocidadeValida) {
      this.velocidadeAtual = novaVelocidade;
    } else {
      this.velocidadeAtual = delta > 0 ? this.velocidadeMaxima : 0;
    }

    return this.velocidadeAtual;
  }

  public acelerar(): number {
    return this.alterarVelocidade(5);
  }

  public frear(): number {
    return this.alterarVelocidade(-5);
  }
}


const carro1 = new Carro('Ford', 'Ka', 185);

// console.log( carro1.acelerar() ) // 5
// console.log( carro1.acelerar() ) // 10
// console.log( carro1.acelerar() ) // 15


Array(50).fill(0).forEach(() => carro1.acelerar());
console.log( carro1.acelerar() ); // 185


Array(20).fill(0).forEach(() => carro1.frear());
console.log( carro1.frear() ); // 80
```

### Herança \#01

```ts
class Carro {
  private velocidadeAtual: number = 0;

  constructor(public marca: string, public modelo: string, private velocidadeMaxima: number = 200) {}

  protected alterarVelocidade(delta: number): number { // OBS: protected
    const novaVelocidade = this.velocidadeAtual + delta;
    const velocidadeValida = novaVelocidade >= 0 && novaVelocidade <= this.velocidadeMaxima;

    if (velocidadeValida) {
      this.velocidadeAtual = novaVelocidade;
    } else {
      this.velocidadeAtual = delta > 0 ? this.velocidadeMaxima : 0;
    }

    return this.velocidadeAtual;
  }

  public acelerar(): number {
    return this.alterarVelocidade(5);
  }

  public frear(): number {
    return this.alterarVelocidade(-5);
  }
}


const carro1 = new Carro('Ford', 'Ka', 185);

Array(50).fill(0).forEach(() => carro1.acelerar());
console.log( carro1.acelerar() ); // 185

Array(20).fill(0).forEach(() => carro1.frear());
console.log( carro1.frear() ); // 80


/**
 * Herança
 */
class Ferrari extends Carro {
  public acelerar(): number {
    return this.alterarVelocidade(20);
  }

  public frear(): number {
    return this.alterarVelocidade(-15);
  }
}


const f40 = new Ferrari('Ferrari', 'F40', 324);

console.log(f40.marca, f40.modelo);

console.log( f40.acelerar() ) // 20
console.log( f40.frear() ) // 5
```

### Herança \#02

Continuação do exemplo acima

```ts
class Ferrari extends Carro {
  constructor(modelo: string, velocidadeMaxima: number) { // OBS: super(...)
    super('Ferrari', modelo, velocidadeMaxima);
  }

  public acelerar(): number {
    return this.alterarVelocidade(20);
  }

  public frear(): number {
    return this.alterarVelocidade(-15);
  }
}


const f40 = new Ferrari('F40', 324);

console.log(f40.marca, f40.modelo);

console.log( f40.acelerar() ) // 20
console.log( f40.frear() ) // 5
```

### Getters & Setters

```ts
class Pessoa {
  private _idade: number = 0

  get idade(): number {
    return this._idade;
  }

  set idade(valor: number) {
    if (valor >= 0 && valor <= 120) {
      this._idade = valor
    }
  }
}


const pessoa1 = new Pessoa;
pessoa1.idade = 10;

console.log( pessoa1.idade ); // 10

pessoa1.idade = -3;
console.log( pessoa1.idade ); // 10
```

### Membros estáticos

Exemplo de um caso que **não é necessário usar instâncias:**

```ts
class Matematica {
  PI: number = 3.1416

  areaCirc(raio: number): number {
    return this.PI * raio * raio;
  }
}


const m1 = new Matematica
m1.PI = 4.2
console.log(m1.areaCirc(4))


const m2 = new Matematica
console.log(m2.areaCirc(4))
```

**O exemplo acima com static:**

```ts
class Matematica {
  static PI: number = 3.1416

  static areaCirc(raio: number): number {
    return this.PI * raio * raio;
  }
}

console.log( Matematica.areaCirc(4) )
```

### Classe Abstrata

#### Explicação do professor sobre classe abstrata
Celular é um conceito abstrato, ninguém tem o 'celular *CELULAR*',
o que você tem é um modelo específico de determinada empresa. Você tem um objeto real que atenta o conceito que está 
debaixo desse conceito chamado celular.

**Exemplo:** Você tem um iPhone X, Samsung S10, S9, S8, *etc..*, motoroloa X, *etc..*, então você têm objetos concretos que estão debaixo desse
conceito chamado **celular**. Mas o 'celular *CELULAR*' mesmo é um conceito abstrato.

Quando você tem uma classe com a palavra `abstract`, você ganha algumas coisas e perde outras coisas.


Não é possível instancia-la:

```ts
abstract class X {}

/**
  error TS2511: Cannot create an instance of an abstract class.
  new X
 */
new X;
```

Tem duas posibilidades, criar um método abstrato e um concreto:

```ts
abstract class X {
  abstract y(a: number): number

  w(b: number): void { // Método concreto
    console.log(b)
  }
}
```

#### Importante
É obrigatório importar métodos abstratos, como o método `executar()` por exemplo. Se não importar, gerará este erro:

```bash
error TS2515: Non-abstract class 'Soma' does not implement inherited abstract member 'executar' 
from class 'Calculo'.

class Soma extends Calculo {}
```

```ts
abstract class Calculo {
  protected resultado: number = 0

  abstract executar(...numeros: number[]): void

  getResultado(): number {
    return this.resultado;
  }
}

class Soma extends Calculo { // O erro aqui 
}

```

#### Modo certo:

```ts
abstract class Calculo {
  protected resultado: number = 0

  abstract executar(...numeros: number[]): void

  getResultado(): number {
    return this.resultado;
  }
}


class Soma extends Calculo {
  executar(...numeros: number[]): void {
    this.resultado = numeros.reduce((t, a) => t + a);
  }
}

class Multiplicacao extends Calculo {
  executar(...numeros: number[]): void {
    this.resultado = numeros.reduce((t, a) => t * a);
  }
}

// Tipar 'Calculo' é opcional
const soma: Calculo = new Soma();
soma.executar(2, 3, 4, 5);

console.log(soma.getResultado()) // 14


// Tipar 'Calculo' é opcional
const multiplicacao = new Multiplicacao();
multiplicacao.executar(2, 3, 4, 5);

console.log(multiplicacao.getResultado()) // 120
```

#### Exemplo de erros:

```ts
let soma1 = new Soma();
/**
  error TS2696: The 'Object' type is assignable to very few other types. 
  Did you mean to use the 'any' type instead?
  Type 'Object' is missing the following properties from type 'Soma': executar, resultado, getResultado

  46 soma1 = new Object;
 */
soma1 = new Object; // Erro aqui. O soma1 foi instanciada pelo tipo "Soma"


let soma2: Calculo = new Soma();
/**
  error TS2696: The 'Object' type is assignable to very few other types. 
  Did you mean to use the 'any' type instead?
  Type 'Object' is missing the following properties from type 'Calculo': 
    resultado, executar, getResultado
 */
soma2 = new Object; // Erro aqui. O soma2 foi instanciada pelo tipo "Calculo"
```

### Constructor Privado & Singleton

*O professor deu um exemplo de criação de um array com conexões de banco de dados*

```ts
class Unico {
  private static instance: Unico = new Unico;

  private constructor() {}

  static getInstance(): Unico {
    // return this.instance;  'this' também serve
    return Unico.instance;
  }

  agora() {
    return new Date;
  }
}

console.log( Unico.getInstance().agora() )
```

#### Chamando de forma errada

```ts
/**
  error TS2673: Constructor of class 'Unico' is private and only accessible within 
  the class declaration.

  19 const errado = new Unico()
 */

const errado = new Unico()
```

### Atributos somente leitura

Somente o `constructor()` pode setar as variáveis `readonly`

```ts
class Aviao {
  public readonly modelo: string

  constructor(modelo: string, public readonly prefixo: string) {
    this.modelo = modelo;
  }

  setModelo(): void {
    this.modelo = 10; // Dá errado.
  }
}


const turboHelice = new Aviao('T-114', 'PT-ABC');

turboHelice.modelo = 'DC-8';    // Dá errado
turboHelice.prefixo = 'PT-DEF'; // Dá errado
```

### Introdução a Namescapes

#### Exemplo sem namespace

**Arquivo:** *namespaces/namespaces.ts*

```ts
const PI = 3.14;

function areaCircunferencia(radio: number): number {
  return PI * Math.pow(radio, 2);
}

function areaRetangulo(base: number, altura: number): number {
  return base * altura;
}
```

#### Exemplo com namespace

**Arquivo:** *namespaces/namespaces.ts*

**OBS:** A constante PI *- fora do escopo Areas -* não irá interferir na constante interna do namespace

```ts
namespace Areas {
  const PI = 3.14;

  export function circunferencia(radio: number): number {
    return PI * Math.pow(radio, 2);
  }

  export function retangulo(base: number, altura: number): number {
    return base * altura;
  }
}

const PI = 2.70; // A constante PI não irá interferir na constante interna do namespace

console.log( Areas.circunferencia(10) ) // 314
console.log( Areas.retangulo(10, 20) ) // 200
```

### Namespaces Aninhados

```ts
namespace Geometria {
  export namespace Area {
    const PI = 3.14;

    export function circunferencia(radio: number): number {
      return PI * Math.pow(radio, 2);
    }

    export function retangulo(base: number, altura: number): number {
      return base * altura;
    }
  }
}

const PI = 2.70;

console.log( Geometria.Area.circunferencia(10) ); // 314
console.log( Geometria.Area.retangulo(10, 20) ); // 200
```

### Namespaces em múltiplos arquivos

**Arquivo:** *namespaces/geometriaCirc.ts*

```ts
namespace Geometria {
  export namespace Area {
    const PI = 3.14;

    export function circunferencia(radio: number): number {
      return PI * Math.pow(radio, 2);
    }
  }
}
```

**Arquivo:** *namespaces/geometriaRect.ts*

```ts
namespace Geometria {
  export namespace Area {
    export function retangulo(base: number, altura: number): number {
      return base * altura;
    }
  }
}
```

**Arquivo:** *namespaces/namespaces.ts*

```ts
console.log( Geometria.Area.circunferencia(10) ) // 314
console.log( Geometria.Area.retangulo(10, 20) ) // 200
```


### Namespaces imports

**Arquivo:** *namespaces/namespaces.ts*

```ts
///<reference path="geometriaRect.ts"/>
///<reference path="geometriaCirc.ts"/>

const PI = 2.70;

console.log( Geometria.Area.circunferencia(10) ) // 314
console.log( Geometria.Area.retangulo(10, 20) ) // 200
```

### Módulos (Seção das aulas Namespaces)

**Arquivo:** *modulos/circunferencia.ts*

```ts
export const PI = 3.14;

export function areaCircunferencia(raio: number): number {
  return raio * raio * PI;
}
```

**Arquivo:** *modulos/retangulo.ts*

```ts
export function areaRetangulo(base: number, altura: number): number {
  return base * altura;
}
```

**Arquivo:** *modulos/modulos.ts*

```ts
import { areaRetangulo } from './retangulo';
import { areaCircunferencia } from './circunferencia';

console.log( areaCircunferencia(10) ) // 314
console.log( areaRetangulo(10, 200) ) // 200

```

#### Erro no browser e no Node não

**Browser:**

```bash
circunferencia.js:2 Uncaught ReferenceError: exports is not defined
    at circunferencia.js:2
(anonymous) @ circunferencia.js:2
retangulo.js:2 Uncaught ReferenceError: exports is not defined
    at retangulo.js:2
(anonymous) @ retangulo.js:2
modulos.js:2 Uncaught ReferenceError: exports is not defined
    at modulos.js:2
```

**Node:**

No Node deu certo porque o node interpreta `exports`

```bash
# npm i -D ts-node
➜  Curso-TypeScript git:(master) ✗ npx ts-node modulos/modulos.ts
314
2000
```

### Instalando SystemJS 0.X

**Comentário do professor:** Em um cenário mais profissional, você não irá usar SystemJS 0.X e sim Webpack, etc...

### Carregando módulos com SystemJS

Fiz alterações no *index.html*

### Importantdo e Exportando Módulos

Alteração nos arquivos:

- modulos/circunferencia.ts
- modulos/modulos.ts
- modulos/retangulo.ts

### A propriedade "module" no TSConfig

```json
{
  "compilerOptions": {
    "module": "commonjs", /* Specify module code generation: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', or 'ESNext'. */
  }
}
```

### Usando Padrão CommonJS

Usando o pacote `@types/node`, é possível usar `module.exports`:

```ts
module.exports = {
  digaOi(nome: string) {
    return 'Oi ' + nome;
  }
};
```

### Resumo: Namespaces vs Módulos

|Namespaces|Módulos|
|:---|:---|
|Organização feita com objetos|Organização feita com módulos reais|
|Pode ser separado em vários arquivos|APP poder ter múltiplos Módulos|
|Não há necessidade de loaders|Precisa de um loader|
|**Gerenciumaneto de depedências torna-se complicado em aplicações grandes**|Declaração explícita de dependências|


### O básico sobre Interfaces

**Lembre-se:** O `interface` nunca vai ser compilado para JS, é apenas um sistema de checagem dentro do TypeScript

#### O problema - DRY *'Don't repeat yourself'*

Percebe-se que ambos as funções contêm parâmetros iguais: `pessoa: { nome: string }`

```ts
function saudarComOla(pessoa: { nome: string }) {
  console.log('Olá,', pessoa.nome);
}

function mudarNome(pessoa: { nome: string }) {
  pessoa.nome = 'Joana';
}


const pessoa = {
  nome: 'João',
  idade: 27
}

saudarComOla(pessoa);
mudarNome(pessoa);
saudarComOla(pessoa);
```

#### Com Interface

```ts
interface Humano {
  nome: string
}

function saudarComOla(pessoa: Humano) {
  console.log('Olá,', pessoa.nome);
}

function mudarNome(pessoa: Humano) {
  pessoa.nome = 'Joana';
}


const pessoa = {
  nome: 'João',
  idade: 27
}

saudarComOla(pessoa); // Olá, João
mudarNome(pessoa);
saudarComOla(pessoa); // Olá, Joana
```

### Interfaces e Propriedades
 
- `?:` é opcional
- `[prop: string]: any` aceitar os demais atributos/propriedades, exemplo o `xyz` e `altura` 

```ts
interface Humano {
  nome: string, // é obrigatório
  idade?: number // ?: é opcional
  [prop: string]: any // Para aceitar os demais atributos/propriedades, exemplo o `xyz` e `altura`
}

function saudarComOla(pessoa: Humano) {
  console.log('Olá,', pessoa.nome);
}

function mudarNome(pessoa: Humano) {
  pessoa.nome = 'Joana';
}


const pessoa = {
  nome: 'João',
  idade: 27
}

saudarComOla(pessoa); // Olá, João
mudarNome(pessoa);
saudarComOla(pessoa); // Olá, Joana

saudarComOla({ nome: 'Jonas', idade: 30, xyz: true, altura: 1.75 });
```

### Interfaces e Métodos

```ts
interface Humano {
  nome: string  // é obrigatório
  idade?: number // ?: é Opcional
  [prop: string]: any // Para aceitar os demais atributos, exemplo o `xyz` e `altura`
  saudar(sobrenome: string): void // é obrigatório
}

function saudarComOla(pessoa: Humano) {
  console.log('Olá,', pessoa.nome);
}

function mudarNome(pessoa: Humano) {
  pessoa.nome = 'Joana';
}


const pessoa: Humano = { // 'Humano' só para garantir
  nome: 'João',
  idade: 27,
  saudar(sobrenome: string) {
    console.log('Olá, meu nome é', this.nome, sobrenome)
  }
}

saudarComOla(pessoa); // Olá, João
mudarNome(pessoa);
saudarComOla(pessoa); // Olá, Joana

// saudarComOla({ nome: 'Jonas', idade: 30, xyz: true, altura: 1.75 });

pessoa.saudar('Skywalker') // Olá, meu nome é Joana Skywalker
```

### Interface e Classes

**OBS:** Interface `Humano` é do código acima

```ts
class Cliente implements Humano {
  nome: string = ''
  ultimaCompra: Date = new Date

  saudar(sobrenome: string) {
    console.log('Olá, meu nome é', this.nome, sobrenome);
  }
}


const meuCliente = new Cliente();
meuCliente.nome = 'Han';
saudarComOla(meuCliente); // Olá, Han
meuCliente.saudar('Solo') // Olá, meu nome é Han Solo
console.log( meuCliente.ultimaCompra ) // Tue May 21 2019 18:43:54 GMT-0300 (Horário Padrão de Brasília)
```

### Interface e Tipo Função

Nome `a` e `b` não importa, o que importa é a quantidade de parâmentros

```ts
interface FuncaoCalculo {
  (a: number, b:number): number
}

let potencia: FuncaoCalculo

potencia = function(base: number, exp: number): number {
  // Math.pow(3, 10)
  // 3 ** 10

  return Array(exp).fill(base).reduce((t, a) => t * a); // É apenas uma alterativa do professor, mas não recomendada
}

console.log(potencia(3, 10));  // 59049
console.log(Math.pow(3, 10));  // 59049
console.log(3 ** 10);          // 59049
```

### Herança com Interfaces
 
#### Exemplo básico de herança

```ts
interface A {
  a(): void
}

interface B {
  b(): void
}

interface ABC extends A, B {
  c(): void
}
```

#### Exemplo com Classes, Funções e Classes Abstratas (abstract)

**Lembre-se:** O `interface` nunca vai ser compilado para JS, é apenas um sistema de checagem dentro do TypeScript

```ts
interface A {
  a(): void
}

interface B {
  b(): void
}

interface ABC extends A, B {
  c(): void
}


// Com Classes
//
class RealA implements A {
  a(): void {}
}

class RealAB implements A, B {
  a(): void {}
  b(): void {}
}

class RealABC implements ABC {
  a(): void {}
  b(): void {}
  c(): void {}

}

// Com Funções
//
function teste(b: B) {

}

teste(new RealABC)


// Com Classes Abstratas
//
abstract class AbstrataABD implements A, B {
  a(): void {}
  b(): void {}
  abstract d(): void
}
```

### Uso de Interface para estender Object (importante)

#### Exemplo com puro JavaScript

```js
Object.prototype.log = function() {
  console.log(this.toString());
}

const x = 2;
const y = 3;
const z = 4;

console.log(x); // 2
console.log(y); // 3
console.log(z); // 4

x.log(); // 2
y.log(); // 3
z.log(); // 4


const cli = { 
  nome: 'Pedro', 
  toString() { return this.nome } 
};

cli.log(); // Pedro
```

**Log com erros do exemplo com JavaScript puro dento do Typescript:**

```bash
error TS2339: Property 'log' does not exist on type 'Object'.

7 Object.prototype.log = function() {}
                   ~~~

error TS2339: Property 'log' does not exist on type '2'.

19 x.log(); // 2
     ~~~

error TS2339: Property 'log' does not exist on type '3'.

20 y.log(); // 3
     ~~~

error TS2339: Property 'log' does not exist on type '4'.

21 z.log(); // 4
     ~~~

error TS2339: Property 'log' does not exist on type '{ nome: string; toString(): string; }'.

29 cli.log(); // Pedro
       ~~~

[19:12:26] Found 5 errors. Watching for file changes.
```


#### Resolvendo o exemplo acima do Typescript

**Lembre-se:** O `interface` nunca vai ser compilado para JS, é apenas um sistema de checagem dentro do TypeScript
 
```ts

// Comparando com código acima, foi acrescentado apenas aqui
//
interface Object {
  log(): void
}


Object.prototype.log = function() {
  console.log(this.toString());
}

const x = 2;
const y = 3;
const z = 4;

console.log(x); // 2
console.log(y); // 3
console.log(z); // 4

x.log(); // 2
y.log(); // 3
z.log(); // 4


const cli = {
  nome: 'Pedro',
  toString() { return this.nome }
};

cli.log(); // Pedro
```


## Generics

Usando a base do exemplo abaixo e com Generics, o compilador vai dizer *"Você está tentando acessar coisa que não pertence ao Number"*

### Por que usar?

`console.log(echo(27).length); ` retornou `undefined` porque não houve nenhum tipo validação para dizer *"length não está disponível no Number"*

```ts
function echo(objeto: any) {
  return objeto;
}

console.log(echo('João').length);
console.log(echo(27).length);      // <- Erro undefined foi aqui 
console.log(echo({ nome: 'João', idade: 27 }));
```

### Criando uma função com Generics (importante)

Usando o `<T>`, o Typescript vai descobrir as propriedades do tipo que foi passado pelo parâmentro. <br>
**Exemplo:** o `number` não tem a propriedade `length`, então o Typescript vai emitir um erro.

```ts
function echoMelhorado<T>(objeto: T): T {
  return objeto;
}

console.log(echoMelhorado('João').length);
console.log(echoMelhorado(27).length); // error TS2339: Property 'length' does not exist on type '27'.
console.log(echoMelhorado<number>(27));
console.log(echoMelhorado({ nome: 'João', idade: 27 }));
```

**Não precisa ser um `<T>`, pode ser qualquer nome:**
 
```ts
function echoMelhorado<TIPO>(objeto: TIPO): TIPO {
  return objeto;
}
```

#### Exemplo com imagens:

**Com number:** <br>  
![Exemplo com number](./images/generics1.jpg)

**Com string:** <br>  
**OBS:** O `String.match(//)` que contém em strings:
<br>
![Exemplo com number](./images/generics2.jpg)


### Usando Generics com Array #01

**Comentário do professor:** Como usar o Generics para especificar o tipo associado a esse Array?

Erro no `Array` abaixo:

```ts
               // error TS2314: Generic type 'Array<T>' requires 1 type argument(s).
const avaliacoes: Array = [10, 9.3, 7.7]; 
               // ~~~~~ //

avaliacoes.push(6.4);
avaliacoes.push('5.5');

console.log(avaliacoes);
```

Com `Array<number>`, causará um no "Fulano", porque é uma string.

```ts
const avaliacoes: Array<number> = [10, 9.3, 7.7];

avaliacoes.push(6.4);

             // error TS2345: Argument of type 'Fulano' is not assignable to parameter of type 'number'.
avaliacoes.push('Fulano');
             // ~~~~~ //

console.log(avaliacoes);
```

#### Corretamente

```ts
// Generics disponíveis na API
let avaliacoes: Array<number> = [10, 9.3, 7.7];

avaliacoes.push(6.4);
// avaliacoes.push('5.5');

console.log(avaliacoes);
```

### Usando Generics com Array #02

#### Exemplo com `type` também

```ts
function imprimir<T>(args: T[]): void {
  args.forEach(elemento => console.log(elemento));
}


imprimir([1, 2, 3, 4]);
imprimir<number>([1, 2, 3, 4]);
imprimir<string>(['Fulano', 'Cicrano', 'Beltrano']);
imprimir<{ nome: string, idade: number }>([
  { nome: 'Fulano', idade: 22 },
  { nome: 'Cicrano', idade: 23 },
  { nome: 'Beltrano', idade: 24 }
])

type Aluno = { nome: string, idade: number };

imprimir<Aluno>([
  { nome: 'Fulano', idade: 22 },
  { nome: 'Cicrano', idade: 23 },
  { nome: 'Beltrano', idade: 24 }
])
```

### Tipo Função com Generics

```ts
const chamarEcho: <T>(data: T) => T = echoMelhorado;

console.log( chamarEcho<string>('Alguma coisa') );
```

Com `type`:

```ts
type Echo = <T>(data: T) => T;

const chamarEcho: Echo = echoMelhorado;

console.log( chamarEcho<string>('Alguma coisa') );
```

### Criando Classes com Generics #01

#### O problema
`this.operando1 + this.operando2` não está validando os tipos. **Solução** na próxima aula

```ts
class OperacaoBinaria {
  constructor(public operando1: any, public operando2: any) {}

  executar() {
    return this.operando1 + this.operando2; // Aqui não está validando
  }
}

console.log(new OperacaoBinaria('Bom ', 'dia').executar());  // Bom dia
console.log(new OperacaoBinaria(3, 7).executar());           // 10
console.log(new OperacaoBinaria(3, ' Opa!').executar());     // 30 Opa!
console.log(new OperacaoBinaria({}, {}).executar());         // [object Object] [object Object]
```

### Criando Classes com Generics #02

#### Com erro

```ts
class OperacaoBinaria<T> {  
  constructor(public operando1: T, public operando2: T) {}

  executar() {
          // error TS2365: Operator '+' cannot be applied to types 'T' and 'T'.
    return this.operando1 + this.operando2;
  }
}

console.log(new OperacaoBinaria('Bom ', 'dia').executar());  // Bom dia
console.log(new OperacaoBinaria(3, 7).executar());           // 10

          // error TS2345: Argument of type '" Opa!"' is not assignable to parameter of type 'number'.
console.log(new OperacaoBinaria(3, ' Opa!').executar()); 
                                 // ~~~~~~ //

console.log(new OperacaoBinaria({}, {}).executar());         // [object Object] [object Object]
```

#### Corretamente

```ts
abstract class OperacaoBinaria<TIPO, RETORNO> {
  constructor(public operando1: TIPO, public operando2: TIPO) {}

  abstract executar(): RETORNO
}

class SomaBinaria extends OperacaoBinaria<number, number> {
  executar(): number {
    return this.operando1 + this.operando2;
  }
}

// Aqui gerou um erro no compilador, porque os argumentos são strings
console.log(new SomaBinaria('Bom ', 'dia').executar()); 

console.log(new SomaBinaria(3, 4).executar());          // 7
```

### Criando Classes com Generics #03

```ts
abstract class OperacaoBinaria<TIPO, RETORNO> {
  constructor(public operando1: TIPO, public operando2: TIPO) {}

  abstract executar(): RETORNO
}


class Data {
  constructor(public dia: number = 1, public mes: number = 1, public ano: number = 1970) {}
}


class DiferencaEntreDatas extends OperacaoBinaria<Data, string> {
  getTime(data: Data): number {
    let { dia, mes, ano } = data;

    return new Date(`${mes}/${dia}/${ano}`).getTime();
  }

  executar(): string {
    const t1 = this.getTime(this.operando1);
    const t2 = this.getTime(this.operando2);
    const diferenca = Math.abs(t1 - t2);
    const dia = 1000 * 60 * 60 * 24; // milissegundos em um dia

    return `${Math.ceil(diferenca / dia)} dia(s)`;
  }
}


const d1 = new Data(1, 2, 2020);
const d2 = new Data(5, 2, 2020);

console.log(new DiferencaEntreDatas(d1, d2).executar()); // 4 dia(s)
```

### Restrições (Constraints)

#### Restringindo tipos

```ts
class Fila<T extends string> {
  constructor(public a: T) {}

  getContent(): T {
    return this.a;
  }
}

const fila = new Fila('Fila ABC');
const fila2 = new Fila<string>('Fila ABC 2');

console.log( fila.getContent() )
console.log( fila2.getContent() )
```

#### Aqui causará um erro no compilador

```ts
const fila3 = new Fila<number>('Fila ABC 2'); // Passei o <number>
const fila4 = new Fila(123); // Passei um valor tipo number
```

## Decorators

### Criando Decorator de Classe

É necessário ativar a opção no *tsconfig.json*:

**Arquivo:** *tsconfig.json*

```json
{
  "compilerOptions": {  
    /* Experimental Options */
    "experimentalDecorators": true
  }
}
```

#### Entendendo

A função `logarClasse` receberá o `constructor` da classe `Eletrodomestico` como parâmetro. <br>
A classe `Eletrodomestico` será convertido internamente pela função `logarClasse`.

O decorator só será chamado quando a classe for chamada.

**OBS:** O decorator `logarClasse` não está sendo chamado por **quantidade de instâncias** e sim quando a classe foi carregada pela primeira vez.

```ts
@logarClasse
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}

function logarClasse(constructor: Function) {
  console.log(constructor);
  /**
    Console.log acima:
      class Eletrodomestico {
          constructor() {
              console.log('novo...');
          }
      }
  */
}

new Eletrodomestico(); // novo...
new Eletrodomestico(); // novo...
new Eletrodomestico(); // novo...
```

**Console.log** do exemplo acima:

```bash
class Eletrodomestico {
    constructor() {
        console.log('novo...');
    }
}
decorators.js:10 novo...
decorators.js:10 novo...
decorators.js:10 novo...
```

### Decorator Factory (importante)

- Quando o `@logarClasseSe(true)` está **true**, a função `logarClasse` será chamada.
- Quando o `@logarClasseSe(false)` está **false**, a função `decoratorVazio` será chamada.

```ts
@logarClasseSe(true) // AQUI
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}

function logarClasse(constructor: Function) {
  console.log(constructor);
}

function decoratorVazio(_: Function) {}

function logarClasseSe(valor: boolean) {
  return valor ? logarClasse : decoratorVazio;
}
```

#### Outro cenário: Tudo numa função só

O decorator `@decorator('Teste', 123)`

```ts
// @logarClasse
// @logarClasseSe(true)
@decorator('Teste', 123)
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}

function logarClasse(constructor: Function) {
  console.log(constructor);
}

function decoratorVazio(_: Function) {}

function logarClasseSe(valor: boolean) {
  return valor ? logarClasse : decoratorVazio;
}


function decorator(a: string, b: number) {
  return function(_: Function): void {
      console.log(a, b); // Teste 123
  }
}
```

#### Outro cenário com objeto no parâmetro

```ts
// @logarClasse
// @logarClasseSe(true)
// @decorator('Teste', 123)
@decorator({a: 'Teste', b: 123})
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}

// ...

function decorator(obj: { a: string, b: number }) {
  return function(_: Function): void {
      console.log(obj.a, obj.b); // Teste 123
  }
}
```

#### Opcional

Parâmetro `b` como opcional

```ts
@decorator({a: 'Teste'})
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}

// ...

function decorator(obj: { a: string, b?: number }) { // Aqui
  return function(_: Function): void {
      console.log(obj.a, obj.b); // Teste undefined
  }
}
```


### Alterando constructor com Decorator de Classe

O professor explica que não é possível chamar o decorator a cada instância, mas é possível chamar a cada instância.

Essa é uma assinatura de um construtor mais genérico possível:

```ts
type Construtor = { new(...args: any[]): {} };
```

É possível usar o tipo nos decorators da aula passada:

```ts
type Construtor = { new(...args: any[]): {} };

// ....

function logarClasse(constructor: Construtor) { // OBS aqui
  console.log(constructor);
}
```

#### Explicando e entendendo o poder do decorator (importante)

O decorator `logarObjeto` será chamada apenas uma única vez e vai substituir a classe `Eletrodomestico` por uma subclasse no 
momento que ele for carregado nessa única vez.

Dentro da decorator é possível fazer várias coisas, como por exemplo:

* Adicionar novos métodos
* Adicionar atributos
* Criar um construtor e interceptar antes e depois a criação do objeto que
    realmente vai criar, que neste exemplo é o Eletrodomestico
* Você tem todo um poder para estender a capacidade dessa classe,
    apenas colocando uma simples decorator: '@logarObjeto'

```ts
@logarObjeto
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}

type Construtor = { new(...args: any[]): {} };

// * Este decorator será chamada apenas uma única vez e
//    vai substituir essa classe por uma subclasse
//    no momento que ele for carregado nessa única vez.
function logarObjeto(constructor: Construtor) {
  return class extends constructor {
    // Aqui dentro é possível fazer várias coisas:
    //  * Adicionar novos métodos
    //  * Adicionar atributos
    //  * Criar um construtor e interceptar antes e depois a criação do objeto que
    //      realmente vai criar, que neste exemplo é o Eletrodomestico
    //  * Você tem todo um poder para estender a capacidade dessa classe,
    //      apenas colocando uma simples decorator: '@logarObjeto'
  }
}
```

```ts
@logarObjeto
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}

type Construtor = { new(...args: any[]): {} };


function logarObjeto(constructor: Construtor) {
  console.log('Carregado...');

  return class extends constructor {
    constructor(...args: any[]) {

      console.log('Antes...');

      super(...args);

      console.log('Depois...');
    }
  }
}

new Eletrodomestico();
new Eletrodomestico();
new Eletrodomestico();
```

**Console.log** do código acima:

```bash
decorators.js:33 Carregado...
decorators.js:36 Antes...
decorators.js:26 novo...
decorators.js:38 Depois...
decorators.js:36 Antes...
decorators.js:26 novo...
decorators.js:38 Depois...
decorators.js:36 Antes...
decorators.js:26 novo...
decorators.js:38 Depois...
```

### Adicionando método com Decorator de Classe (prototype)

Alterando os protótipos da classe. <br>
No caso abaixo contém um erro no `.imprimir()` e a solução está logo abaixo:

```ts
@imprimivel
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}

function imprimivel(constructor: Function) {
  constructor.prototype.imprimir = function() {
    console.log(this);
  }
}

new Eletrodomestico().imprimir() // Eletrodomestico {}
```

Solução aqui:

```ts
(<any>new Eletrodomestico()).imprimir() // Eletrodomestico {}
```

Com interface:

```ts
interface Eletrodomestico {
  imprimir?(): void
}

@imprimivel
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}


function imprimivel(constructor: Function) {
  constructor.prototype.imprimir = function() {
    console.log(this);
  }
}


const eletro = new Eletrodomestico();
eletro.imprimir && eletro.imprimir();
```

### Múltiplos Decorators

**OBS:** Os `@logarObjeto` e `@imprimivel` funcionam normalmente

```ts
// ...
@logarObjeto
@imprimivel
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}

// ...

const eletro = new Eletrodomestico();
eletro.imprimir && eletro.imprimir();
```

### Decorator de Método

```ts
class ContaCorrente {
  private saldo: number

  constructor(saldo: number) {
    this.saldo = saldo;
  }

  sacar(valor: number) {
    if (valor <= this.saldo) {
      this.saldo -= valor;
      return true;
    } else {
      return false;
    }
  }

  getSaldo() {
    return this.saldo;
  }
}


const cc = new ContaCorrente(10254.78);
cc.sacar(5000);
console.log( cc.getSaldo() ) // 5254.780000000001

// Simulando alguém mal intencionado queira substituir o método,
//  aplicando uma nova função com nova lógica
cc.getSaldo = function() {
  return this['saldo'] + 7000;
}

console.log( cc.getSaldo() ) // 12254.78
```

Impedindo alguém substituir o método:

Adiconado `@congelar` em cima dos método para ser insubstituíveis.

O `PropertyDescriptor` é um tipo do JS. Aqui está sendo usada a propriedade `.writable`, mais informações:
https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty#Descri%C3%A7%C3%A3o

```ts
class ContaCorrente {
  private saldo: number

  constructor(saldo: number) {
    this.saldo = saldo;
  }

  @congelar
  sacar(valor: number) {
    if (valor <= this.saldo) {
      this.saldo -= valor;
      return true;
    } else {
      return false;
    }
  }

  @congelar
  getSaldo() {
    return this.saldo;
  }
}

/**
  Aqui será gerado um erro, porque o método não pode ser reinscrito.
 */
cc.getSaldo = function() {
  return this['saldo'] + 7000;
}

// Object.freeze()
function congelar(alvo: any, nomePropriedade: string, descritor: PropertyDescriptor) {
  console.log(1, alvo);            // {constructor: ƒ, sacar: ƒ, getSaldo: ƒ}
  console.log(2, nomePropriedade); // sacar
  descritor.writable = false;
}
```

### Decorator de Atributo

Decorator `@naoNengativo`

```ts
class ContaCorrente {
  @naoNegativo
  private saldo: number

  constructor(saldo: number) {
    this.saldo = saldo;
  }

  @congelar
  sacar(valor: number) {
    if (valor <= this.saldo) {
      this.saldo -= valor;
      return true;
    } else {
      return false;
    }
  }

  @congelar
  getSaldo() {
    return this.saldo;
  }
}


const cc = new ContaCorrente(10248.57);
cc.sacar(5000);
cc.sacar(5248.57);
cc.sacar(5248.57);
cc.sacar(-0.1);

console.log( cc.getSaldo() ) // 5248.57

// Object.freeze()
function congelar(alvo: any, nomePropriedade: string, descritor: PropertyDescriptor) {
  console.log(1, alvo);            // {constructor: ƒ, sacar: ƒ, getSaldo: ƒ}
  console.log(2, nomePropriedade); // sacar
  descritor.writable = false;
}


function naoNegativo(alvo: any, nomePropriedade: string) {
  delete alvo[nomePropriedade];

  Object.defineProperty(alvo, nomePropriedade, {
    get: function(): any {
      return alvo["_" + nomePropriedade]
    },
    set: function(valor: any): void {
      if (valor < 0) {
        throw new Error('Saldo Inválido');
      } else {
        alvo["_" + nomePropriedade] = valor;
      }
    }
  });
}
```

### Decorator de Parâmetro

Derocator aplicado em: `sacar(@paramInfo valor: number) {}`

```ts
class ContaCorrente {
  @naoNegativo
  private saldo: number

  constructor(saldo: number) {
    this.saldo = saldo;
  }

  @congelar
  sacar(@paramInfo valor: number) {
    if (valor <= this.saldo) {
      this.saldo -= valor;
      return true;
    } else {
      return false;
    }
  }

  @congelar
  getSaldo() {
    return this.saldo;
  }
}

// ...

function paramInfo(alvo: any, nomeMetodo: string, indiceParam: number) {
  console.log('Alvo: ', alvo);                // Alvo:  {constructor: ƒ, sacar: ƒ, getSaldo: ƒ}
  console.log('Método: ', nomeMetodo);        // Método:  sacar
  console.log('Indice Param: ', indiceParam); // Indice Param:  0
}
```

## Usando bibliotecas JS com Typescript - Declare

### Usando jQuery

No caso abaixo pode funcionar no JS, mas o Typescript não reconhecerá o `$` e com isso provocará um erro no compilador

O tipo `$` não foi criado, por isso causará erro no compilador.

**Arquivo:** *bibliotecas/bibliotecas.ts*

```js
$('body').append('Usando jQuery');
```

### Declarando variável com "Declare"

Esta solução pode resolver os tipos, mas não resolverá os tipos internos da biblioteca. <br>
Aqui para o problema de compilação, entretando, não está resolvendo os tipos interno da biblioteca

```ts
declare const $: any;

$('body').append('Usando jQuery');
```

### Entendendo os Arquivos de Declaração de Tipos

Esta alternativa é fazer a separação em um arquivo específico. Para isso, é o arquivo `*.d.ts` (esse `d` vem do "definition")

**Arquivo:** *bibliotecas/jquery.d.ts*

```ts
declare const $: any;
```

**Arquivo:** *bibliotecas/bibliotecas.ts*

```js
$('body').append('Usando jQuery');
```

**Professor acessou o site:** https://www.typescriptlang.org/docs/handbook/declaration-files/by-example.html

No site acima tem exemplos:
- Como criar esse arquivo(*.d.ts) 
- Como declarar determinadas variáveis
- Como declarar um função

**OBS:** Se for biblioteca de mercado, provavelmente já tem a biblioteca. O professor vai ensinar na próxima aula.

**OBS:** Caso queira integrar uma biblioteca da empresa, é interessantes olhar a documentação acima.

### Usando definicação de tipos públicas(de "mercado")

No exemplo, o professor instalou `npm i -D @types/jquery`:

```bash
npm i -D @types/jquery
```

Com isso, ele vai resolver os "dollars"(`$` do jquery), mas os tipos internos do jQuery

E também, vai aparecer os detalhes parâmetros que o `.append()` aceita

![Exemplo com jQuery](./images/biblioteca1.jpg)

**E os método por exemplo:**

![Exemplo com jQuery](./images/biblioteca2.jpg)

#### Procurando tipos (importante)

É possível procurar tipos em: https://microsoft.github.io/TypeSearch/

Fazendo busca no Google:
- *'typescript declaration jquery'*


## Integração TypeScript + VueJS

### Configurando projeto VueJS com TypeScript

**vuejs/vue-class-component:** https://github.com/vuejs/vue-class-component <br>
**Vue Property Decorator:** https://github.com/kaorun343/vue-property-decorator

**O professor** fez os seguintes comandos:

```bash
$ sudo npm i -g @vue/cli
$ vue create integracao-vue
$   ❯ Manually select features

# Abaixo escolheu: Babel, TypeScript e Linter/Formatter
$    ◉ Babel
    ❯◉ TypeScript
     ◯ Progressive Web App (PWA) Support
     ◯ Router
     ◯ Vuex
     ◯ CSS Pre-processors
     ◉ Linter / Formatter
     ◯ Unit Testing
     ◯ E2E Testing
$   ? Use class-style component syntax? (Y/n) # Respondeu que Sim
$   ? Use Babel alongside TypeScript for auto-detected polyfills? (Y/n) # Respondeu que Sim

# Abaixo escolheu: TSLint
$   ? Pick a linter / formatter config: (Use arrow keys)
    ❯ TSLint 
      ESLint with error prevention only 
      ESLint + Airbnb config 
      ESLint + Standard config 
      ESLint + Prettier

# Abaixo escolheu: Lint on save
$ ? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i> to invert selection)
      ❯◉ Lint on save
       ◯ Lint and fix on commit

# Abaixo escolheu: In dedicated config files
$   ? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? (Use arrow keys)
      ❯ In dedicated config files 
        In package.json 

$   ? Save this as a preset for future projects? (y/N) # Respondeu que Não


### INSTALAÇÃO ABAIXO:
### INSTALAÇÃO ABAIXO:
  Creating project in /home/eduardo/Documentos/integracao-vue.
🗃  Initializing git repository...
⚙  Installing CLI plugins. This might take a while...

# .....

⚓  Running completion hooks...

📄  Generating README.md...

🎉  Successfully created project integracao-vue.
👉  Get started with the following commands:

 $ cd integracao-vue
 $ npm run serve
```

#### Projeto criado em:

https://gitlab.com/duduindo/integracao-vue-typescript

Rode os comandos e acesse http://localhost:8080/ 

```bash
$ cd integracao-vue
$ npm run serve

DONE  Compiled successfully in 4548ms                                                                                                                 17:45:09

Type checking and linting in progress...

  App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://192.168.2.200:8080/

  Note that the development build is not optimized.
  To create a production build, run npm run build.

No type errors found
No lint errors found
Version: typescript 3.4.5, tslint 5.16.0
Time: 4872ms
```

## Partir daqui, estou mexendo no projeto:

https://gitlab.com/duduindo/integracao-vue-typescript
