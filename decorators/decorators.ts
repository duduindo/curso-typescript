function logarClasse(constructor: Function) {
  console.log(constructor);
}

function decoratorVazio(_: Function) {}

function logarClasseSe(valor: boolean) {
  return valor ? logarClasse : decoratorVazio;
}


function decorator(obj: { a: string, b: number }) {
  return function(_: Function): void {
      console.log(obj.a, obj.b); // Teste 123
  }
}



type Construtor = { new(...args: any[]): {} };


function logarObjeto(constructor: Construtor) {
  console.log('Carregado...');

  return class extends constructor {
    constructor(...args: any[]) {

      console.log('Antes...');

      super(...args);

      console.log('Depois...');
    }
  }
}


interface Eletrodomestico {
  imprimir?(): void
}

@logarObjeto
@imprimivel
class Eletrodomestico {
  constructor() {
    console.log('novo...');
  }
}


function imprimivel(constructor: Function) {
  constructor.prototype.imprimir = function() {
    console.log(this);
  }
}

const eletro = new Eletrodomestico();
eletro.imprimir && eletro.imprimir();




class ContaCorrente {
  @naoNegativo
  private saldo: number

  constructor(saldo: number) {
    this.saldo = saldo;
  }

  @congelar
  sacar(@paramInfo valor: number) {
    if (valor <= this.saldo) {
      this.saldo -= valor;
      return true;
    } else {
      return false;
    }
  }

  @congelar
  getSaldo() {
    return this.saldo;
  }
}


const cc = new ContaCorrente(10248.57);
cc.sacar(5000);
cc.sacar(5248.57);
cc.sacar(5248.57);
cc.sacar(-0.1);

console.log( cc.getSaldo() ) // 5248.57

// Object.freeze()
function congelar(alvo: any, nomePropriedade: string, descritor: PropertyDescriptor) {
  console.log(1, alvo);            // {constructor: ƒ, sacar: ƒ, getSaldo: ƒ}
  console.log(2, nomePropriedade); // sacar
  descritor.writable = false;
}


function naoNegativo(alvo: any, nomePropriedade: string) {
  delete alvo[nomePropriedade];

  Object.defineProperty(alvo, nomePropriedade, {
    get: function(): any {
      return alvo["_" + nomePropriedade]
    },
    set: function(valor: any): void {
      if (valor < 0) {
        throw new Error('Saldo Inválido');
      } else {
        alvo["_" + nomePropriedade] = valor;
      }
    }
  });
}



function paramInfo(alvo: any, nomeMetodo: string, indiceParam: number) {
  console.log('Alvo: ', alvo);                // Alvo:  {constructor: ƒ, sacar: ƒ, getSaldo: ƒ}
  console.log('Método: ', nomeMetodo);        // Método:  sacar
  console.log('Indice Param: ', indiceParam); // Indice Param:  0
}
